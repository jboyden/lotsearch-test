#!/usr/bin/env python3
#
# Load `input/Dataset_update.csv`;
# use it according to the specification in `input/spec.txt`:
#
#       The provided csv --- "Dataset_update.csv" contains a list of datasets
#       and Pkeys(Primary Keys) that correspond to tables in a database.
#       with a "QA" field that needs to be updated to a value of 3 .
#
#       Write a python script that reads the tables(dataset) and
#       primary key(pkeys) columns from the csv and updates the QA column
#       for the corresponding pkeys.
#
# Usage:
#   python3 solution_q1.py

import csv
import os
import re
import sqlite3
import sys

import create_test_db

from collections import namedtuple
from itertools import groupby
from typing import Sequence


# The filename of the input CSV file.
#
# (If this were a longer-lived script, I'd set up command-line parsing
# using `argparse`, so this could be specified on the command-line.)
_INPUT_CSV_FNAME = 'input/Dataset_update.csv'

# Whether to ignore the first line in the input CSV file
# (because it's column titles / field names rather than
# the same format as the data rows).
#
# (If this were a longer-lived script, I'd set up command-line parsing
# using `argparse`, so this could be specified on the command-line.)
_IGNORE_FIRST_CSV_ROW = True

# The regex format to validate each field of each non-title data row
# of the CSV file.  If the CSV data format changes, this is what you'll
# need to review/update.
#
# The group names in this regex must correspond to the field names
# in the namedtuple `CsvRow`; if one is modified, the other should be
# modified accordingly.
#
# The CSV data appears to have the format:
#   State,[DatasetTableName],PrimaryKey
#
# Note the square brackets around the dataset table name;
# they will need to be verified and removed.
#
# Also, the primary keys appear to be integers in all the tables.
_CSV_ROW_FORMAT = re.compile(
        r'^(?P<state>[A-Za-z]+),[[](?P<dataset>[A-Za-z]+)[]],(?P<pkey>\d+)$')

# For longer-lived code, I find namedtuples more maintainable than builtin
# Python tuples (especially if the tuple len > 2, and/or the tuple instances
# cross module boundaries) and much quicker to write than a full-blown class
# with a useful `__repr__` etc.
#
# Fields:
#  - `state`: the name of an Australian state (UPPERCASE alphabetic in data)
#  - `dataset`: the name of a table in the DB (MixedCase alphabetic in data)
#  - `pkey`: a primary key (integer in data)
#
# The field names in this namedtuple must correspond to the group names
# in the regex `_CSV_ROW_FORMAT` defined just above; if one is modified,
# the other should be modified accordingly.
CsvRow = namedtuple('CsvRow', 'state dataset pkey')


def demo_solution(db_fname=None):
    """Demonstrate the solution according to the spec.

    If specified database filename `db_fname` is `None`, ask `create_test_db`
    to create a new test DB with its default test DB filename.  The default
    test DB filename will be returned for subsequent re-use in this module.
    Otherwise (if `db_fname` is not `None`), assume that the test DB has
    already been created (with the specified database filename `db_fname`).

    Return the CSV rows that were read from the input CSV file.
    It consists of a list of `CsvRow` instances that contain verified data.
    """
    csv_rows = read_input_csv(_INPUT_CSV_FNAME, _IGNORE_FIRST_CSV_ROW)

    # Rather than executing an SQL 'UPDATE' statement for every row of
    # the CSV file, let's instead group together the CSV rows by table name;
    # then we can update all pkeys for a given table together.
    grouped_csv_rows = _group_csv_rows(csv_rows, 'dataset')

    if db_fname is None:
        db_fname = create_test_db.create_new_test_db(db_fname)
    _update_column_in_tables(db_fname, grouped_csv_rows, 'QA', 3)

    return csv_rows


def _update_column_in_tables(db_fname, grouped_csv_rows, column_name, new_val):
    db_con = sqlite3.connect(db_fname)
    db_cur = db_con.cursor()

    total_num_rows_updated = 0
    for (table_name, csv_rows) in grouped_csv_rows:
        pkey_list = ', '.join([str(row.pkey) for row in csv_rows])
        update_sql = \
                'UPDATE {tab} SET {col} = {val} WHERE Pkey IN ({pkeys})'.format(
                tab=table_name, col=column_name, val=new_val, pkeys=pkey_list)

        print(update_sql)
        db_cur.execute(update_sql)
        total_num_rows_updated += len(csv_rows)

    # Don't forget to commit the transaction!
    db_con.commit()

    db_cur.close()
    db_con.close()

    print('\n%d rows updated in total (in %d tables)' %
            (total_num_rows_updated, len(grouped_csv_rows)))


def _group_csv_rows(csv_rows, group_by_field):
    # First, sort the CSV rows by the specified field,
    # so that runs of any field value will be contiguous.
    by_field = lambda row: getattr(row, group_by_field)
    sorted_csv_rows = sorted(csv_rows, key=by_field)

    # Now group the sorted CSV rows by the specified field.
    result = []
    for (field, group) in groupby(sorted_csv_rows, key=by_field):
        result.append((field, list(group)))
    return result


def read_input_csv(input_csv_fname, ignore_first_csv_row=False):
    """Read the input CSV file specified by filename `input_csv_fname`.

    Return a list of `CsvRow` instances that contain verified data.

    If optional argument `ignore_first_csv_row` is True, ignore the first line
    of the CSV file (the first parsed CSV row), under the assumption that it's
    column titles / field names rather than actual data.
    """
    with open(input_csv_fname) as input_file:
        csv_rows = csv.reader(input_file, delimiter=',')
        if ignore_first_csv_row:
            # Take and ignore the first row of the `csv.reader` iterable.
            # (We use `next()` because the `csv.reader` iterable is not
            # slice-able like a list.)
            ignored_first_row = next(csv_rows)

        return [_validate_csv_row(r) for r in csv_rows]


def _validate_csv_row(csv_row: Sequence[str]):
    """Validate a single parsed CSV row, return a single `CsvRow` instance.

    Argument to `csv_row` is assumed to be a list of strings, because that's
    what the `csv.reader` iterable returns per-row.

    This function uses regex `_CSV_ROW_FORMAT` to validate the format & values
    of the CSV row, before performing any appropriate type-conversions, before
    returning a `CsvRow` instance.
    """
    # We need to re-join the parsed CSV fields into a single string,
    # or run them each through their own regex.  At least `csv.reader`
    # has validated the basic comma-separated CSV format for us.
    m = _CSV_ROW_FORMAT.match(','.join(csv_row))
    if not m:
        raise ValueError('invalid format in CSV row: %s' % csv_row)

    state = m.group('state')
    dataset = m.group('dataset')
    pkey = int(m.group('pkey'))
    return CsvRow(state=state, dataset=dataset, pkey=pkey)


def die(message, exit_status=-1):
    """Terminate the program with specified `message` and `exit_status`.

    The error message is printed to stderr.
    """
    print('%s\nAborted.' % message, file=sys.stderr)
    sys.exit(exit_status)


if __name__ == '__main__':
    demo_solution()
