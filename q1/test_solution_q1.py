#!/usr/bin/env python3
#
# Create a temporary test SQLite DB; populate it with test data; and
# then test `solution_q1.py` using this newly-created DB of test data.
#
# Usage:
#   python3 test_solution_q1.py
#
# Note: Because this script is validating the output of the actual solution,
# I'm allowing myself to write terser Python code than I normally would.
# (Lots of `assert`, etc.)

import os
import sqlite3

import create_test_db
import solution_q1


def test_solution():
    test_db_fname = create_test_db.create_new_test_db()
    tables_to_test = create_test_db.list_tables_to_test()

    rows_before = _select_all_data_from_tables(test_db_fname, tables_to_test)
    rows_before.sort()

    # And now, the moment I've all been waiting for.
    input_csv_rows = solution_q1.demo_solution(test_db_fname)

    rows_after = _select_all_data_from_tables(test_db_fname, tables_to_test)
    rows_after.sort()

    print('\n%d DB rows before; %d DB rows after' %
            (len(rows_before), len(rows_after)))
    assert (len(rows_before) == len(rows_after))

    rows_that_differ = [
            (before, after)
            for (before, after) in zip(rows_before, rows_after)
            if (before != after)]

    num_rows_that_differ = sum(1 for _ in rows_that_differ)
    print('\n%d input CSV rows; %d rows differ (before-vs-after)' %
            (len(input_csv_rows), num_rows_that_differ))
    assert (len(input_csv_rows) == num_rows_that_differ)

    # Ensure that the table name never differs before-vs-after.
    assert all((before[0] == after[0]) for (before, after) in rows_that_differ)
    # Ensure that the primary key never differs before-vs-after.
    assert all((before[1] == after[1]) for (before, after) in rows_that_differ)

    # Ensure that the "QA" field *always* differs before-vs-after.
    assert all((before[2] != after[2]) for (before, after) in rows_that_differ)

    # Ensure that dummy field #1 never differs before-vs-after.
    assert all((before[3] == after[3]) for (before, after) in rows_that_differ)
    assert all((before[3] == 'dummy') for (before, after) in rows_that_differ)
    # Ensure that dummy field #2 never differs before-vs-after.
    assert all((before[4] == after[4]) for (before, after) in rows_that_differ)
    assert all((before[4] == 222) for (before, after) in rows_that_differ)

    table_name_pkey_from_csv = [(c.dataset, c.pkey) for c in input_csv_rows]
    table_name_pkey_from_csv.sort()

    table_name_pkey_of_diffs = [(b[0], b[1]) for (b, a) in rows_that_differ]
    table_name_pkey_of_diffs.sort()

    assert (table_name_pkey_from_csv == table_name_pkey_of_diffs)
    # And thus, we've confirmed that the only differences in the database
    # are the "QA" field changes in the rows specified in the input CSV file.
    print('\nAll differences in the database correspond to the input CSV file.')


def _select_all_data_from_tables(db_fname, table_names, *, flatten=True):
    db_con = sqlite3.connect(db_fname)
    db_cur = db_con.cursor()
    result = []
    for table_name in table_names:
        select_sql = 'SELECT * FROM %s' % table_name
        rows = list(db_cur.execute(select_sql))
        if flatten:
            # The result will be a single flat (long!) list of tuples:
            #   (table_name, the fields of a row in this table)
            #
            # This long list can then be sorted and compared before-vs-after.
            result.extend([
                    ((table_name,) + row) for row in rows])
        else:
            # The result will be a list (foreach table) of lists (foreach row).
            result.append((table_name, rows))

    db_cur.close()
    db_con.close()

    return result


if __name__ == '__main__':
    # If `execute_sql` is False, we instead perform a dry run:
    # Just print SQL statements to stdout, don't execute any SQL.
    test_solution()
