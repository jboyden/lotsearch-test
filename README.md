Test submission by James Boyden
===============================

Question 1:
-----------

The specification for Question 1:

> The provided csv --- `"Dataset_update.csv"` contains a list of datasets
> and Pkeys(Primary Keys) that correspond to tables in a database.
> with a `"QA"` field that needs to be updated to a value of 3 .
> 
> Write a python script that reads the tables(dataset) and primary key(pkeys)
> columns from the csv and updates the QA column for the corresponding pkeys.

### Submitted Python code

There are 3 Python files in the submission for this question:

- `solution_q1.py`: the Python script that was requested in the spec
- `test_solution_q1.py`: a Python script for me (or you) to test my solution
- `create_test_db.py`: Create a test DB in SQLite3; populate it with test data.

There are 3 different ways you can run these Python modules as scripts:

- `python3 solution_q1.py`: Create a test DB in SQLite3;
  run the requested solution on this test DB, as in the spec.
- `python3 test_solution_q1.py`: Create a test DB in SQLite3;
  test the correctness of the requested solution by a
  before-vs-after comparison of the rows in the test DB.
- `python3 create_test_db.py`: Just create a stand-alone test DB in SQLite3.

### SQLite3 test DB

These scripts use an
[SQLite3 database](https://www.sqlite.org/index.html) to contain test data.
The [Python3 SQLite3 module](https://docs.python.org/3/library/sqlite3.html)
is part of the Python3 standard library; but the underlying `sqlite3` library
might need to be installed manually.
A key benefit of an SQLite3 database is that it exists entirely within a
single file in the current directory: It is compact, easily-created, and
easily-deleted, making it ideal for temporary SQL databases that are set-up
and torn-down during testing.

By default, the SQLite3 test database will be created in a file `_test.db`
in the current directory; this default filename may be configured in the
global variable `_TEST_DB_FNAME` in module `create_test_db.py`.
(If this were a longer-lived script, I'd set up command-line parsing using
Python's `argparse`, so this filename could be specified on the command-line.)

- **Note** that after any of these 3 database-creating scripts have been run,
  the created test database file `_test.db` is retained in the current directory
  for any subsequent inspection you wish to perform.
- **Note** that the database-creating scripts will refuse to create the test DB
  file if it already exists in the current directory, to avoid accidentally
  overwriting the test results.  So, if you want to run the code again
  (and who wouldn't!), you must delete the file `_test.db` first.

### Testing approach

To enable testing by `test_solution_q1.py`:

In `create_test_db.py`, we extract the lists of tables, columns, and
primary keys from the spec; then we add in a few dummy table names, and
a few dummy columns, and a few dummy primary keys; then we calculate a
Cartesian product of (tables x pkeys) to produce our test data.
This ensures that we have the data to be modified by the SQL `'UPDATE'`
statement mixed in with a variety of other data that should not be modified.

The basic testing approach in `test_solution_q1.py` is:

1. Create a new test DB.
2. Select all data from the DB; flatten all per-table rows into a single list
  that prepends the table names before each row tuple; sort the list of tuples.
3. Run the solution, which should perform an SQL `'UPDATE'` statement.
4. Again, select all data from the DB; again flatten all per-table rows into
  a single list; again sort the list.
5. The two lists should have the same length, since only existing rows should
  have been modified.  **Verify that the lists have the same length.**
6. Compare the two sorted lists pairwise for element-by-element equality:
  Because the lists are already sorted (itself an `O(N log N)` operation),
  comparing the two lists element-wise is an `O(N)` operation, rather than
  something awful like `O(N^2)` if we were trying to match elements
  whack-a-mole style in unsorted lists.
7. **Verify that the only column that has changed is `'QA'`.**
8. Count the number of pairwise differences.  **Verify that every pairwise
  difference corresponds directly to exactly one data row in the input CSV.**

Question 2:
-----------

The specification for Question 2:

> Write a python script that creates a shapefile from the Water Customer
> Connection Points layer where the `D_Diameter = ‘100m’`.
> 
> Please extract the data from the following Map Server end point.
> 
> https://services.thelist.tas.gov.au/arcgis/rest/services/Public/Infrastructure/MapServer/44
> 
> Output spatial reference must be 7855.

and Nigel subsequently clarified in email:

> Please proceed with `D_Diameter = '100mm'`

### Submitted Python code

There is just 1 Python file in the submission for this question: 

- `solution_q2.py`: the Python script that was requested in the spec

There is just 1 way you can run this Python module as a script:

- `python3 solution_q2.py`: Request GeoJSON from the Map Server endpoint
  where `D_DIAMETER='100mm'` and Output Spatial Reference is `7855`;
  convert the GeoJSON response to a Shapefile.

Running this script will produce 5 files in the current directory:

- `_response.geojson`: a GeoJSON file that caches the response
- `Water_D_Diameter_100mm.dbf`
- `Water_D_Diameter_100mm.prj`
- `Water_D_Diameter_100mm.shp`: our Shapefile, which can be loaded in QGIS
- `Water_D_Diameter_100mm.shx`

### Testing approach

Because we're querying an external resource with a specific set of parameters,
there's not really much testing that can be done, other than:

1. Does the Map Server endpoint request successfully download the requested
  features in GeoJSON format?
2. Does the GeoJSON file contain **more than zero** (specifically: 1006)
  `Point` features where property `"D_DIAMETER" = "100mm"`?
  - Use command-line: `grep -i D_DIAMETER _response.geojson`
  - and command-line: `grep -i D_DIAMETER _response.geojson | wc -l`
3. Does the GeoJSON file contain **only** `Point` features where
  property `"D_DIAMETER" = "100mm"`?
  - Use command-line: `grep -i D_DIAMETER _response.geojson | grep -v '"100mm"'`
4. Does `ogrinfo` read (1006) `Point` features in the converted Shapefile?
  - Use command-line: `ogrinfo -al Water_D_Diameter_100mm.shp | grep -i 'Point (' | wc -l`
5. Does the converted Shapefile open and display in QGIS?

Linux package dependencies
--------------------------

The following non-default packages were needed on my Linux Mint system:

- `gdal-bin`
- `markdown`
- `python3-gdal`
- `sqlite3`

These package dependencies are also listed in file `apt-get-install.txt`.
It should be convenient to install these packages using the command-line:

```
sudo apt-get install `cat apt-get-install.txt`
```

Python package dependencies
---------------------------

The following non-default Python packages were needed on my system:

- `requests`

This package dependency is also listed in file `requirements.txt`.
It should be convenient to install this package using the command-line:

```
sudo python3 -m pip install -r requirements.txt
```

(If you're running in a Python virtual environment, you won't need `sudo`
at the start of the command.)

