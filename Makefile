TIMESTAMP = $(shell date '+%Y-%m-%d--%H-%M-%S')

# Take a timestamped work-in-progress snapshot.
# (Normally I'd use git for this.)
snapshot:
	( cd .. && tar cjvf wip-snapshot--$(TIMESTAMP).tar.bz2 Test_submission_James_Boyden )

# Package up the final test submission.
tar: clean
	( cd .. && tar cvf test_submission_james_boyden.tar Test_submission_James_Boyden )

# Regenerate the README.html from markdown.
doc: README.md
	markdown README.md > README.html

# Install any Linux Mint package requirements.
install-apt:
	sudo apt-get install gdal-bin markdown python3-gdal sqlite3

# Install any Python3 package requirements.
install-pip: requirements.txt
	python3 -m pip install -r requirements.txt

# Delete any unnecessary (probably large) temporary files.
clean:
	rm -f q1/_test.db
	rm -rf q1/__pycache__
	rm -f q2/_response.geojson
	rm -f q2/Water_D_Diameter_100mm.dbf
	rm -f q2/Water_D_Diameter_100mm.prj
	rm -f q2/Water_D_Diameter_100mm.shp
	rm -f q2/Water_D_Diameter_100mm.shx
