#!/usr/bin/env python3
#
# The original specification in `Skill Test.docx`:
#
#       Write a python script that creates a shapefile from the Water Customer
#       Connection Points layer where the `D_Diameter = ‘100m’`.
#       
#       Please extract the data from the following Map Server end point.
#       
#       https://services.thelist.tas.gov.au/arcgis/rest/services/Public/Infrastructure/MapServer/44
#       
#       Output spatial reference must be 7855.
#
# and Nigel subsequently clarified in email:
#      
#       Please proceed with `D_Diameter = '100mm'`
#
# Usage:
#   python3 solution_q2.py
#
# Running this script will produce 5 files in the current directory:
#  - `_response.geojson`: a GeoJSON file that caches the response
#  - `Water_D_Diameter_100mm.dbf`
#  - `Water_D_Diameter_100mm.prj`
#  - `Water_D_Diameter_100mm.shp`: our Shapefile, which can be loaded in QGIS
#  - `Water_D_Diameter_100mm.shx`

import json
import os
import requests

from osgeo import gdal, ogr


# The URL of the Map Server endpoint to query.
#
# (If this were a longer-lived script, I'd set up command-line parsing
# using `argparse`, so this could be specified on the command-line.)
_ENDPOINT_URL = 'https://services.thelist.tas.gov.au/arcgis/rest/services/Public/Infrastructure/MapServer/44'

# The output spatial reference to request.
#
# (Yadda yadda `argparse`.)
_SPATIAL_REF = 7855

# The filename of the output Shapefile to create.
#
# (Yadda yadda `argparse`.)
_SHAPEFILE_FNAME = 'Water_D_Diameter_100mm.shp'

# The query parameters for a GET method request.
_QUERY_PARAMS = dict(
        where="D_DIAMETER='100mm'",
        outFields="*",
        returnGeometry=True,
        outSR=_SPATIAL_REF,
        returnIdsOnly=False,
        returnCountOnly=False,
        returnExtentOnly=False,
        #f="pjson",
        f="geojson",
)

# The filename of a GeoJSON (JSON) file in which to cache the response.
# (When you're web-scraping, it's polite to cache the intermediate responses
# so you don't need to hit the remote server any more than necessary.)
_CACHED_JSON_FNAME = '_response.geojson'


def demo_solution():
    if os.path.exists(_CACHED_JSON_FNAME):
        print('Loading cached JSON response: %s' % _CACHED_JSON_FNAME)
        with open(_CACHED_JSON_FNAME) as f:
            resp_json = json.load(f)
    else:
        print('Querying Map Server endpoint: %s' % _ENDPOINT_URL)
        resp_json = _query_map_server(_ENDPOINT_URL, _QUERY_PARAMS)
        print('Saving (caching) JSON response to file: %s' % _CACHED_JSON_FNAME)
        with open(_CACHED_JSON_FNAME, 'w') as f:
            # For reproducability, sort the output of dictionaries by key.
            # Otherwise, even for identical data, the ordering of the keys
            # in a saved dictionary fluctuates randomly,
            # which complicates text-based diffing of saved JSON data.
            json.dump(resp_json, f, indent=1, sort_keys=True)

    _inspect_response_features(resp_json)
    _convert_geojson_to_shp(_SHAPEFILE_FNAME, _CACHED_JSON_FNAME)


def _convert_geojson_to_shp(shp_fname, geojson_fname):
    """Convert a GeoJSON file named `geojson_fname` to a Shapefile `shp_fname`.

    This uses GDAL's `osgeo.ogr`:
     https://gdal.org/api/python/osgeo.ogr.html
    """
    # Silence the following error message from OGR:
    #   ERROR 6: EPSG PCS/GCS code 7855 not found in EPSG support files.
    #   Is this a valid EPSG coordinate system?
    gdal.PushErrorHandler('CPLQuietErrorHandler')

    # https://gdal.org/drivers/vector/geojson.html
    in_driver_geojson = ogr.GetDriverByName('GeoJSON')
    in_data_geojson = in_driver_geojson.Open(geojson_fname)

    # https://gdal.org/drivers/vector/shapefile.html
    out_driver_shp = ogr.GetDriverByName('ESRI Shapefile')
    out_data_shp = out_driver_shp.CopyDataSource(in_data_geojson, shp_fname)


def _inspect_response_features(resp_json):
    """Briefly describe the response features for user information."""
    features = resp_json['features']
    num_features = len(features)
    print('%d features extracted' % num_features)
    if num_features > 0:
        print('features[0] =\n%s' % json.dumps(features[0], indent=1))


def _query_map_server(endpoint_url, query_params):
    """Query the Map Server endpoint specified by `endpoint_url`.

    Provide the query parameters specified in dict `query_params`.

    Return the parsed JSON response (as Python dicts, etc.).
    """
    resp = requests.get('%s/query' % endpoint_url, params=query_params)
    print('Response URL: %s\n' % resp.url)
    return resp.json()


if __name__ == '__main__':
    demo_solution()
